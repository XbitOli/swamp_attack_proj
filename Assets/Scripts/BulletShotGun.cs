using UnityEngine;

public class BulletShotGun : Bullet
{
    Vector2 _direction;

    private void Awake()
    {
        _direction = new Vector2(-1, Mathf.Sin(Random.Range(Mathf.PI * 5.5f / 6.0f, 7.0f * Mathf.PI / 6.1f)));
    }
    private void Update() 
    {
        transform.Translate(_direction * _speed * Time.deltaTime, Space.World);
    }
}
