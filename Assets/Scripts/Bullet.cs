using UnityEngine;

public abstract class Bullet : MonoBehaviour
{
    [SerializeField] protected int _damage;
    [SerializeField] protected float _speed;

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.TryGetComponent<Enemy>(out Enemy enemy))
            enemy.TakeDamage(_damage);

        Destroy(gameObject);
    }
}
