using UnityEngine;

public class Shotgun : Weapon
{
    private int _numberShot = 8;
    public override void Shoot(Transform shootPoint)
    {
        for (int i = 0; i < _numberShot; i++)
        {
            Instantiate(bullet, new Vector3(shootPoint.position.x, shootPoint.position.y, shootPoint.position.z), Quaternion.identity);
        }
    }
}
